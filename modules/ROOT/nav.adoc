//* xref:docs-team.adoc[Who we are]
* Who we are

//* xref:organizational/index.adoc[How we work]
* How we work
** xref:organizational/charter.adoc[The Docs Charter]
** xref:organizational/meetings.adoc[Docs Meetings]
** xref:organizational/workflow.adoc[Docs Workflow organization]

// * xref:locations.adoc[Where to find]
* Where to find

* xref:contributing-docs/index.adoc[Write contributions to Docs]

** Contributing to existing documentation 
// xref:contributing-docs/contrib-existing-documentation.adoc[Contributing to existing documentation]
** xref:contributing-docs/contrib-release-notes.adoc[Contributing to Release Notes]
** xref:contributing-docs/contrib-new-documentation.adoc[Contributing a new documentation site]

** Contribution Tools
*** xref:contributing-docs/tools-7step-gitlab-howto.adoc[7 Step HowTo for casual contributions (for GitLab-based pages)]
// ** xref:contributing-docs/7step-pagure-howto.adoc[7 Step HowTo for casual contributions (for Pagure-based pages)]
*** HowTo for casual contributions (for Pagure-based pages)
//** xref:contributing-docs/use-web-ide-ui.adoc[How to profoundly use the Gitlab web IDE (permanent members)]
*** How to profoundly use the Gitlab web IDE (permanent members)
//** xref:contributing-docs/use-file-edit-if.adoc[How to profoundly use Pagure (permanent members)]
*** xref:contributing-docs/tools-local-authoring-env.adoc[How to create and use a local Fedora authoring environment]
*** xref:contributing-docs/tools-vale-linter.adoc[How to check documentation style with Vale]  

** xref:contributing-docs/style-guide.adoc[The docs style guide]
** xref:contributing-docs/asciidoc-intro.adoc[AsciiDoc for Fedora]
*** xref:contributing-docs/asciidoc-markup.adoc[Markup]
*** xref:contributing-docs/asciidoc-reusable-attributes.adoc[Reusable attributes]
// ** xref:contributing-docs/translations.adoc[Working with Translations]
** Working with Translations


//* Contribute to keeping Docs up and running
* xref:contributing-infra/index.adoc[Contribute to keeping docs up and running]
** xref:contributing-infra/design-ux-contribution.adoc[Design the user interface for Docs]


//* xref:archive/index.adoc[What we have achieved so far]
* What we have achieved so far

